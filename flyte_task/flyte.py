
import typing
import functools
import flytekitplugins.pandera  # noqa : F401
import pandas as pd
import pandera as pa
from flytekit import task, workflow
from pandera.typing import DataFrame, Series
import os


BAZ = 14

class Foo():
    def __init__(self):
        print("I")

    def bar(self):
        print("")


def log_io(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        print(f"task {fn.__name__} called with args: {args}, kwargs: {kwargs}")
        # print("OS ENV A", os.environ["A"])
        out = fn(*args, **kwargs)
        print(f"task {fn.__name__} output: {out}")
        return out

    return wrapper


import datetime


class InSchema(pa.SchemaModel):
    hourly_pay: Series[float] = pa.Field(ge=7)
    hours_worked: Series[float] = pa.Field(ge=10)

    @pa.check("hourly_pay", "hours_worked")
    def check_numbers_are_positive(cls, series: Series) -> Series[bool]:
        """Defines a column-level custom check."""
        return series > 0


class IntermediateSchema(InSchema):
    total_pay: Series[float]

    @pa.dataframe_check
    def check_total_pay(cls, df: DataFrame) -> Series[bool]:
        """Defines a dataframe-level custom check."""
        return df["total_pay"] == df["hourly_pay"] * df["hours_worked"]


class OutSchema(IntermediateSchema):
    worker_id: Series[str] = pa.Field()


@task(container_image="flyte:latest")
def dict_to_dataframe(data: dict) -> DataFrame[InSchema]:
    """Helper task to convert a dictionary input to a dataframe."""
    return pd.DataFrame(data)


def mytask(*args, **kwargs):
    print(f"0000000000000000000000 {args} {kwargs}")
    def decorator_task(func):
        @task
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)
        return wrapper
    return decorator_task


def decorator(func):
    @functools.wraps(func)
    def wrapper_decorator(*args, **kwargs):
        print("test decorator")
        value = func(*args, **kwargs)
        # Do something after
        return value
    return wrapper_decorator


def repeat(num_times):
    def decorator_repeat(func):
        @functools.wraps(func)
        def wrapper_repeat(*args, **kwargs):
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value
        return wrapper_repeat
    return decorator_repeat


def mytask(*args, betteromics=True, **kwargs):
    print("Betteromics")
    return task(*args, **kwargs)


from flytekit.extras.tasks.shell import OutputLocation, ShellTask
import time


shell_task = ShellTask(
    name="shell_task_1",
    debug=True,
    script="""
    set -ex
    echo "Hey there! Let's run some bash scripts using Flyte's ShellTask."
    echo $A
    """
)

# THIS WORKS!


def mytask2(*args, betteromics=False, **kwargs):
    print("123", args, kwargs)
    def inner(func):
        @task(*args, **kwargs, environment={"A": "B"})
        @functools.wraps(func)  # Not required, but generally considered good practice
        def wrapper(*args, **kwargs):
            if betteromics:
                print("Betteromics was here", args, kwargs)
                # print("INSIDE WRAPPER", os.environ["A"])
            return func(*args, **kwargs)
        return wrapper
    return inner


def mytask3(*args, betteromics=False, **kwargs):
    print("321", args, kwargs)
    def inner(func):
        @task(*args, **kwargs, environment={"A": "B"})
        @functools.wraps(func)  # Not required, but generally considered good practice
        def wrapper(*args, **kwargs):
            if betteromics:
                print("Betteromics was here", args, kwargs)
                # print("INSIDE WRAPPER", os.environ["A"])
            return func(*args, **kwargs)
        return wrapper
    return inner

# @mytask2(betteromics=True, disable_deck=True, timeout=1)


@mytask2(betteromics=True, container_image="flyte:latest")
def total_pay(df: DataFrame[InSchema]) -> typing.Tuple[DataFrame[IntermediateSchema], Foo]:  # noqa : F811
    # time.sleep(2)
    # print("INSIDE TASK", os.environ["A"])
    return df.assign(total_pay=df.hourly_pay * df.hours_worked), Foo()


@task(timeout=datetime.timedelta(seconds=1), environment={"A": "B"}, cache=True, cache_version="1", container_image="flyte:latest")
def add_ids(
    df: DataFrame[IntermediateSchema], worker_ids: typing.List[str]
) -> DataFrame[OutSchema]:
    # print("!!!!!", os.environ["A"])
    time.sleep(10)
    return df.assign(worker_id=worker_ids)


@task(timeout=datetime.timedelta(seconds=60), environment={"A": f"{BAZ}"}, cache=True, cache_version="1", container_image="custom:v3")
def foobar(
    a: int = 0
) -> int:
    print("!!!!!memow22222454!!!!!!!!!", os.environ["A"])
    import pandas
    import numpy 
    import seaborn
    import uuid
    import common.proto.job_pb2 as job_proto
    time.sleep(10)
    a = a + 10
    return a + 1

@task(timeout=datetime.timedelta(seconds=60), environment={"A": "C"}, cache=True, cache_version="1", container_image="custom:v1")
def foobar2(
    a: int = 0
) -> int:
    print("!!!!!memowSEEEEE", os.environ["A"])
    time.sleep(10)
    a = a + 10
    return a + 1

@task(timeout=datetime.timedelta(seconds=60), environment={"A": "B"}, cache=True, cache_version="1")
def foobar3(
    a: int = 0
) -> int:
    print("!!!!!memow", os.environ["A"])
    time.sleep(10)
    a = a + 10
    return a + 1

@task(timeout=datetime.timedelta(seconds=60), environment={"A": "B"}, cache=True, cache_version="1", container_image="flytebasics:v1")
def foobarcopy(
    a: int = 0
) -> int:
    print("!!!!!memow22222", os.environ["A"])
    time.sleep(10)
    a = a + 10
    return a + 1


@workflow
def run_foobar_basic(  # noqa : F811
    a: int = 0
) -> int:
    # test = shell_task()
    temp = foobar(a=a)
    return temp

@workflow
def run_foobar2_latest(  # noqa : F811
    a: int = 0
) -> int:
    # test = shell_task()
    temp = foobar2(a=a)
    return temp

@workflow
def run_foobar3_default(  # noqa : F811
    a: int = 0
) -> int:
    # test = shell_task()
    temp = foobar3(a=a)
    return temp


# @workflow
# def process_data(  # noqa : F811
#     data: dict = {
#         "hourly_pay": [12.0, 13.5, 10.1],
#         "hours_worked": [30.5, 40.0, 41.75],
#     },
#     worker_ids: typing.List[str] = ["a", "b", "c"],
# ) -> DataFrame[OutSchema]:
#     # test = shell_task()
#     some_val = foobar(a=2)
#     return add_ids(df=total_pay(df=dict_to_dataframe(data=data))[0], worker_ids=worker_ids)


@repeat(num_times=4)
def bar(a):
    print("HI", a)


from flytekit import LaunchPlan, current_context, task, workflow


if __name__ == "__main__":
    print(f"Running {__file__} main...")
    # bar(1)
    # result = process_data(
    #     data={"hourly_pay": [12.0, 13.5, 10.1], "hours_worked": [30.5, 40.0, 41.75]},
    #     worker_ids=["a", "b", "c"],
    # )
    # print(f"Running wf(), returns dataframe\n{result}\n{result.dtypes}")

    # default_lp = LaunchPlan.get_default_launch_plan(current_context(), process_data)
    # square_3 = default_lp(
    #     data={"hourly_pay": [12.0, 13.5, 10.1], "hours_worked": [30.5, 40.0, 41.75]},
    #     worker_ids=["a", "b", "c"],
    # )
