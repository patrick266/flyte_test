import functools


def BetterTask(func, export=True):
    if export:
        print("Hi")
    @functools.wraps(func)
    def wrapper_decorator(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper_decorator


@BetterTask
def foo(a):
    print(a)


foo(1)
