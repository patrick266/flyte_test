import data_factory.proto.pipeline_pb2 as pipeline_proto
from data_factory.api.scripts.utils import MercuryClient

client = MercuryClient()
client.init_db_and_rpc_connections()


pipeline_uuid = '60523efa-6a81-4bb9-8ded-e5a5529ecd8f'
pipeline_data_input_type_uuid = '7ffeccd4-e2e3-4734-8305-0b9ca3ff4ff6'
data_source_snapshot_uuid = '23742b39-e31c-476a-8e73-dbf2de2bd76f'

# Add input *twice*
client.pipeline_stub.AddPipelineInput(
    request=pipeline_proto.AddPipelineInputRequest(
        pipeline_uuid=pipeline_uuid,
        pipeline_data_input_type_uuid=pipeline_data_input_type_uuid,
        data_source_snapshot_uuid=,
    )
)
client.pipeline_stub.AddPipelineInput(
    request=pipeline_proto.AddPipelineInputRequest(
        pipeline_uuid=pipeline_uuid,
        pipeline_data_input_type_uuid=pipeline_data_input_type_uuid,
        data_source_snapshot_uuid=,
    )
)

# Find second input, remove
pipeline_data_input_uuid = '42ea0a47-88e9-4a76-b9a4-495a4a1efba3'
client.pipeline_stub.RemovePipelineInput(
    request=pipeline_proto.RemovePipelineInputRequest(
        pipeline_data_input_uuid=pipeline_data_input_uuid,
    )
)

# Start pipeline
client.pipeline_stub.StartPipelines(
    request=pipeline_proto.StartPipelinesRequest(
        pipeline_uuids=[pipeline_uuid]
    )
)

# Get pipeline status
client.pipeline_stub.GetPipeline(
    request=pipeline_proto.GetPipelineRequest(
        uuid=pipeline_uuid
    )
)
